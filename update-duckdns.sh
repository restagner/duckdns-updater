#!/bin/bash
set -euo pipefail
################################################################
# Config these items for your account.
#
DOMAIN=your-domain-name
TOKEN=your-duckdns-token
INTERFACE=your-local-interface
RCFILE=
SILENT=
VERBOSE=
FORCE=
EXTERNAL=
LOGFILE=
AS_SERVICE=

################################################################
# No need to change anything beyond this line.
#
SCRIPT=$(basename $(readlink -f "$0") )
IFCONFIG_CMD=$(type -P ifconfig)
IPADDR_CMD=$(type -P ip)
CURL_CMD=$(type -P curl)
CURL_T='curl -LsS --http2 ${VERBOSE:+-v} https://www.duckdns.org/update/$DOMAIN/$TOKEN'

log() {
  [[ ${VERBOSE:-} || ${LOGFILE:-} ]] || return
  local msg=$(echo "$(date +%FT%T%z) LOG: $*" | sed "s|$TOKEN|${TOKEN//?/x}|g")
  [[ ${VERBOSE:-} ]] && echo "$msg" >&2
  eval "echo '$msg' >> ${LOGFILE:-/dev/null}"
}

die() {
  echo "Error: $*" | tee -a ${LOGFILE:-/dev/null} >&2
  exit 1
}

usage() {
  [[ ${1:--h} = -h ]] || echo "Error: $*" >&2
  cat >&2 <<EOT

Usage:

  ./${SCRIPT##*/} [-hvfnXs] [-T TK] [-D DN] [-I IFACE] [-L FN]

Options:
  -h    Display this message
  -v    Be verbose
  -s    Silence curl output (usually 'OK' acknowledgment line)
  -f    Force update, even if ip addr hasn't changed
  -n    Only display the operations, don't perform them
  -X    Query EXTERNAL ip address instead of local interface addr
  -T TK Use 'TK' as the duckdns token
  -D DN Use 'DN' as the domain name to update
  -I IF Use 'IF' as the local interface's ip address to check
  -L FN Output log messages to 'FN'

Exits with a success/failure exit code and 'OK'/'ERR' output.

Configuration defaults:
  * Domain: ${DOMAIN:-}
  * Token: ${TOKEN:-}
  * Interface: ${INTERFACE:-}
  * Silence curl output: ${SILENT:-no}
  * Run verbosely: ${VERBOSE:-no}
  * Force update: ${FORCE:-no}
  * Use external address: ${EXTERNAL:-no}
  * Log filename: ${LOGFILE:-none}

EOT
  [[ ${1:--h} = -h ]] || exit 1
  exit 0
}

egrep_addr() {
  egrep -o '[0-9]+(\.[0-9]+){3}' "$@" 2>/dev/null | head -1
}

find_address() {
  eval "$1 2>/dev/null" | egrep '^[ ]+inet[ ]' | egrep_addr
}

rc_file() {
  local rc="$HOME/.${SCRIPT%.sh}.rc" # default rcfile
  if [[ ${RCFILE:-} ]]; then
    rc=${RCFILE}
  elif [[ ${UID} -eq 0 && ${AS_SERVICE:-} ]]; then
    rc="/etc/${SCRIPT%.sh}.rc"
  elif [[ -d $HOME/.config ]]; then
    rc="$HOME/.config/${SCRIPT%.sh}.rc"
  elif [[ -d $HOME/.local ]]; then
    rc="$HOME/.local/${SCRIPT%.sh}.rc"
  fi
  echo ${rc}
}

RCFILE=$(rc_file)

current_addr() {
  local ip
  local regex_ip='(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])' 

  if [[ ${EXTERNAL:-} ]]; then
    ip=$(find_address "curl -LsS http://ipconfig.me/")
  elif [[ ${INTERFACE:-your-xxx} = your-* ]]; then
    die "Please specify your interface"
  elif [[ ${IFCONFIG_CMD:-} ]]; then
    ip=$(find_address "ifconfig ${INTERFACE}")
  elif [[ ${IPADDR_CMD:-} ]]; then
    ip=$(find_address "ip addr show ${INTERFACE}")
  else
    die "Unable to query local interface state"
  fi

  if [[ ${ip} =~ ${regex_ip} ]]; then 
  	log "found current address: $ip"
  else
	die "could not find IP address. Verify that the supplied interface, '${INTERFACE}', is correct."
  fi

  echo ${ip:-}
}

update_dns() {
  # $1=new ip addr
  local cmd=$(eval "echo '$CURL_T/$1 ${SILENT:+>/dev/null 2>&1}'")
  if [[ ${NOOP:-} ]]; then
    log "[noop] $cmd"
  elif [[ ${VERBOSE:-} ]]; then
    log "[run] $cmd"
  fi
  if eval "$cmd" ; then
    [[ ${SILENT:-} ]] || echo ""
    log "dns update successful, saving ip address to rc file"
    echo -e "# $(date)\n${1}" > ${RCFILE}
  else
    log "unable to update the dns service"
  fi
}

eval set -- "$(getopt -o hvfnXsT:D:I: -n ${SCRIPT} -- "$@")"
while [[ ${1:-} ]]; do
  case "$1" in
    -h) usage ;;
    -D) DOMAIN="$2"; shift ;;
    -T) TOKEN="$2"; shift ;;
    -I) INTERFACE="$2"; shift ;;
    -L) LOGFILE="$2"; shift ;;
    -s) SILENT=yes ;;
    -v) VERBOSE=yes ;;
    -f) FORCE=yes ;;
    -X) EXTERNAL=yes ;;
    -n) NOOP=yes ;;
    --) shift ; break ;;
  esac
  shift
done

OLD_IP=$(egrep_addr ${RCFILE} || echo 0.0.0.0)
NEW_IP=$(current_addr)
if [[ ${FORCE:-} ]]; then
  log "Detected 'force update' flag, updating dns record"
  update_dns ${NEW_IP}
elif [[ ${OLD_IP} = ${NEW_IP} ]]; then
  log "No ip address change detected, exiting"
else
  log "Detected an ip address change, updating dns record"
  update_dns ${NEW_IP}
fi

# fin
